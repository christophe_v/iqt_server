import sys
import json

if sys.version_info >= (3,):
    import http.client as httplib
else :
    import httplib


conn = httplib.HTTPConnection("localhost:9000")

#conn.request("GET", "/?cmd=status")
#response = conn.getresponse()
#print(res.status, res.reason)

headers = {'Content-type': 'application/json'}
params = {"dummy" : "dummy"}

conn.request('POST', '/post', json.dumps(params), headers)
response = conn.getresponse()
print(response.read().decode())