import time
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
import json

HOST_NAME = 'localhost'
PORT_NUMBER = 9000



class MyHandler(BaseHTTPRequestHandler):

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        
    def do_POST(self):
        self._set_headers()
        parsed_path = urlparse(self.path)
        #request_id = parsed_path.path
        print("POST request, ", self.path, parsed_path)
        response = {"title" : "this is a response"}
        self.wfile.write(json.dumps(response))
     

    def do_GET(self):
        try :
            query = urlparse(self.path).query
            query_components = dict(qc.split("=") for qc in query.split("&"))

        except :
            print("URL parsing error: URL in wrong format!")
            return
        
        commands = ['status']

        if 'cmd' not in query_components or query_components['cmd'] not in commands :
            print('Error: Command not recognized')
            return
        else :
            exec('self.' + query_components['cmd'] + '(' + str(query_components) + ')')

       
    
    def status(self, query_components) :
        print("In status")
        self.respond({'status' : 101})
    

    def handle_http(self, status_code, path):
        self.send_response(status_code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        content = '''
        <html><head><title>Title goes here.</title></head>
        <body><p>This is a test.</p>
        <p>You accessed path: {}</p>
        </body></html>
        '''.format(path)
        return bytes(content, 'UTF-8')
        

    def respond(self, opts):
        response = self.handle_http(opts['status'], self.path)
        self.wfile.write(response)

def main():
    server_class = HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print(time.asctime(), 'Server Starts - %s:%s' % (HOST_NAME, PORT_NUMBER))

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass

    httpd.server_close()
    print(time.asctime(), 'Server Stops - %s:%s' % (HOST_NAME, PORT_NUMBER))
    return 0


if __name__ == '__main__':
    main()




